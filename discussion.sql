INSERT INTO artists(name)
VALUES ("Rivermaya");

-- Mini-activity #1
-- Insert a new record in artists table with value 'Psy';
INSERT INTO artists(name)
VALUES ("Psy");

-- Mini-activity #2
-- Insert new records in albums table with the following values
INSERT INTO albums(album_name, year, artist_id)
VALUES ("Psy 6", 2012-1-1, "2"),
("Trip", 1995-1-1, "1");

-- Retrieving exisiting records
SELECT * FROM artists;

-- Mini-activity #3
-- Insert new records in 'songs' table with the ff values:
INSERT INTO songs(title, length, genre, album_id)
VALUES ('Gangnam Style', '253', 'K-pop', '3'),
('Kundiman', '234', 'OPM', '4'),
('Kisapmata', '319', 'OPM', '4');

-- Other way of Inserting values:
-- INSERT INTO albums (album_name, year, artist_id) VALUES ("Psy 6", "2012-1-1",(SELECT id FROM artists WHERE name ="Psy"));

-- Display the title of all songs.
SELECT title FROM songs;

-- Syntax
-- SELECT <column>,...
-- FROM <table>
-- WHERE <condition>;

-- Display the title of all the 'OPM' songs.
SELECT title
FROM songs
WHERE genre = 'OPM';

-- Display the album name where the artist is "Rivermaya"
SELECT album_name
FROM albums
WHERE artist_id = '1';

-- Syntax
-- SELECT <column>,...
-- FROM <table>
-- WHERE <condition2>
-- AND/OR <condition2>;

-- Display the title and length of the OPM songs that came from Trip album and the length greater than 240
SELECT title, length
FROM songs
WHERE album_id = 4
AND length > 240;

-- Updating records
-- Syntax 
-- UPDATE <table>
-- SET <column> = <new_value>
-- WHERE <condition>;

-- Update the length of kundiman to 240
UPDATE songs
SET length = 240
WHERE id = 2;
-- OR --
UPDATE songs
SET length = 240
WHERE title = 'kundiman';

-- Deleting records
-- Syntax
-- DELETE FROM <table>
-- WHERE <condition>
-- AND|OR <condition>;

-- Delete all OPM songs that are equal to or greater than 2 mins in length
DELETE from songs
WHERE genre = 'OPM'
AND length >= 200;